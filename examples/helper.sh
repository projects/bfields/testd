#!/bin/bash

server=$1
command=$2
shift; shift

case $command in
reboot )
	# A full reboot would also be good to test.
	# But this is faster, and must also work, and avoids some
	# odd problems waiting for the server to come up
	# (on a server with a very short lease period, my previous
	# technique (waiting for ssh to come up) failed because nfsd
	# came up much faster than ssh).
	ssh root@$server "systemctl restart nfs-server.service"
	;;
unlink )
	ssh $server "rm $1"
	;;
rename )
	ssh $server "mv $1 $2"
	;;
link )
	ssh $server "ln $1 $2"
	;;
chmod )
	ssh $server "chmod $1 $2"
	;;
esac
